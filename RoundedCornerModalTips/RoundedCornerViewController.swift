//
//  RoundedCornerViewController.swift
//  RoundedCornerModalTips
//
//  Created by See.Ku on 2014/09/30.
//  Copyright (c) 2014 AxeRoad. All rights reserved.
//

import UIKit

class RoundedCornerViewController: UIViewController {

	@IBOutlet weak var whiteView: UIView!
	@IBOutlet weak var sizeLabel: UILabel!
	@IBOutlet weak var verLabel: UILabel!

	@IBAction func closeModal(sender: AnyObject) {
		dismissViewControllerAnimated(true, completion: nil)
	}

	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()

		if g_roundedCorner {

			let rad = CGFloat(60.0)

			// ViewControllerの角を丸める
			cornerRounding(rad)

			// ついでにViewの角も丸めておく
			whiteView.layer.cornerRadius = rad
			whiteView.layer.masksToBounds = true
		}

		// サイズを表示
		let si = whiteView.bounds.size
		sizeLabel.text = "Size (White): \(si.width)x\(si.height)"
	}

	// ViewControllerの角を丸める
	func cornerRounding(radius: CGFloat) {

		// UINavigationControllerに対応
		var vi = view
		if navigationController != nil {
			vi = navigationController!.view
		}

		// iOS7はこれでだけok
		vi.layer.cornerRadius  = radius
		vi.layer.masksToBounds = true
		vi.superview?.backgroundColor = UIColor.clearColor()

		// iOS8の場合、以下の処理も必要
		if let sv = vi.superview {
			sv.clipsToBounds = false
			sv.layer.shadowPath = nil
		}
	}

	////////////////////////////////////////////////////////////////

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

		let ver = UIDevice.currentDevice().systemVersion
		verLabel.text = "iOS: \(ver)"
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
