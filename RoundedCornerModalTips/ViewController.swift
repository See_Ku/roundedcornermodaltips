//
//  ViewController.swift
//  RoundedCornerModalTips
//
//  Created by See.Ku on 2014/09/30.
//  Copyright (c) 2014 AxeRoad. All rights reserved.
//

import UIKit

var g_roundedCorner = true

class ViewController: UIViewController {

	@IBAction func onSwitchCorner(sender: UISwitch) {
		g_roundedCorner = sender.on
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}


}

